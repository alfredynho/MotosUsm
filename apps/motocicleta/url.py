# -*- encoding: utf-8 -*-

from django.conf.urls import include, url
from apps.motocicleta.forms import MotocicletaCreateView
from apps.motocicleta.forms import MotocicletaUpdateView
from apps.motocicleta.views import MotocicletasListView

from apps.motocicleta.forms import MotocicletaSucursalAjaxCreateView
from apps.motocicleta.forms import MotocicletaSucursalAjaxUpdateView
from apps.motocicleta.views import MotocicletaSucursalListView

from motocicleta.views import MotocicletasListSucursal

urlpatterns = [

    url(r'^motocicleta/crear$', MotocicletaCreateView.as_view(), name='crear'),
    url(r'^motocicleta/(?P<pk>\d+)/$', MotocicletaUpdateView.as_view(), name='actualizar'),
    url(r'^motocicletas/$', MotocicletasListView.as_view(), name='listar-motocicletas'),

    url(r'^motocicletas/sucursal/(?P<spk>\d+)/agregar/(?P<vpk>\d+)/$', MotocicletaSucursalAjaxCreateView.as_view(), name='agregar-motocicleta-sucursal'),
    url(r'^motocicletas/sucursal/actualizar/(?P<pk>\d+)/$', MotocicletaSucursalAjaxUpdateView.as_view(), name='actualizar-motocicleta-sucursal'),
    url(r'^motocicletas/sucursal/(?P<pk>\d+)/$', MotocicletaSucursalListView.as_view(), name='listar-motocicletas-sucursal'),

    url(r'^motocicletas/inventario$', MotocicletasListSucursal.as_view(), name='listar-motocicletas-inventario'),
]
