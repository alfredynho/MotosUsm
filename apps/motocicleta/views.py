from django.shortcuts import render
from .models import Motocicleta
from django.views.generic.detail import DetailView
from apps.sucursal.models import Sucursal, Sucursal_Motocicleta
from .forms import MotocicletaSucursalCreateForm

class MotocicletasListView(ListView):
	"""Lista todos las motocicletas."""
	model = Motocicleta
	context_object_name = "motocicletas" # configurar el context de motocicleta
	template_name = 'vehiculo/vehiculo_list.html'

class MotocicletaSucursalListView(ListView):
	"""Lista las motocicletas por sucursal."""

	model = Sucursal_Motocicleta
	context_object_name = 'sucursal_vehiculos'
	template_name = 'vehiculo/inventario_list.html'

	#funcion para sacar queryset de todas las motocicletas
	def get_queryset(self):
		'''Permite filtrar las motocicletas que seran mostrados.

		Dada la pk de la sucursal se sobre escribe el metodo
		para que se listen los motocicletas que pertenecen a una
		sucursal dada.

		'''
		sucursal_id = self.kwargs['pk']
		sucursal = Sucursal.objects.get(id=sucursal_id)

		sucursal_motocicletas = Sucursal_Motocicleta.objects.filter(sucursal=sucursal)
		return sucursal_motocicletas # retorna todos las motocicletas por sucursal

	def get_context_data(self,**kwargs):
		"""Permite agregar al contexto la sucursal a la cual pertenecen los motocicletas listados
		en el query_set."""

		context = super(MotocicletaSucursalListView,self).get_context_data(**kwargs)
		sucursal_id = self.kwargs['pk']
		sucursal = Sucursal.objects.get(id=sucursal_id)
		context['sucursal'] = sucursal
		context['motocicletas'] = Motocicleta.objects.all()


		return context

# Preguntar a Lisa
class MotocicletasListSucursal(ListView):
 	"""Lista todos los motocicletas con su respectiva sucursal."""

	model = Sucursal_Motocicleta
	context_object_name = 'sucursal_motocicletas'
	template_name = 'vehiculo/inventario_sucursal.html'