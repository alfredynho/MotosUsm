# -*- encoding: utf-8 -*-
#importando las librerias para las vistas basadas en clases
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.template import loader # sirve para renderizar las plantillas
from django.template import Context # para mandar un context a la plantilla
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse
from django.contrib import messages # para mostrar mensajes tipo popup
from django import forms # importando las librerias

from .models import Motocicleta # importando los modelos de Motocicleta
from apps.sucursal.models import Sucursal # importando los modelos la app Sucursal
from apps.sucursal.models import Sucursal_Motocicleta

import json

class MotocicletaCreateView(CreateView):
	model = Motocicleta
	fields = ['numero_serie', 'marca', 'modelo', 'motor', 'potencia', 'tipo',
	'capacidad', 'caracteristicas', 'imagen', 'precio']

	def get_context_data(self,**kwargs):
		context = super(MotocicletaCreateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Nueva Motocicleta'
		context['button_text'] = 'Crear'
		return context

	def get_success_url(self):
		messages.info(self.request,"La motocicleta ha sido creado con exito")
		return reverse_lazy('motocicleta:listar-motocicleta')


class MotocicletaUpdateView(UpdateView):
	model = Motocicleta
	fields = ['numero_serie', 'marca', 'modelo', 'motor', 'potencia', 'tipo',
	'capacidad', 'caracteristicas', 'imagen', 'precio']
	success_url = reverse_lazy('motocicleta:listar-motocicleta')

	def get_context_data(self,**kwargs):
		context = super(MotocicletaUpdateView,self).get_context_data(**kwargs)
		context['section_title'] = 'Actualizar Motocicleta'
		context['button_text'] = 'Actualizar'
		return context

	def get_success_url(self):
		messages.info(self.request,"La motocicleta ha sido actualizado con exito")
		return reverse_lazy('motocicleta:listar-motocicleta')


class MotocicletaSucursalCreateForm(forms.ModelForm):

	class Meta:
		model = Sucursal_Motocicleta
		fields = ('motocicleta','sucursal','color','cantidad','habilitado')


class MotocicletaSucursalAjaxCreateView(TemplateView):

	def get(self,request,*args,**kwargs):

		sucursal_id = Sucursal.objects.get(id=kwargs['spk']).id
		motocicleta_id = Motocicleta.objects.get(id=kwargs['mpk']).id

		template = loader.get_template('includes/form.html')
		form = MotocicletaSucursalCreateForm({
			'sucursal':sucursal_id,
			'motocicleta':motocicleta_id})

		context = {'form':form}
		html = template.render(context)
		response = {
			'status':True,
			'html':html
		}

		data = json.dumps(response)
		return HttpResponse(data,content_type='application/json')

	def post(self,request,*args,**kwargs):
		form = MotocicletaSucursalCreateForm(request.POST)
		if form.is_valid():
			form.save()
			template = loader.get_template('vehiculo/includes/inventario.html')
			motocicletas = Motocicleta.objects.all()
			sucursal = Sucursal.objects.get(id=kwargs['spk'])
			sucursal_motocicletas = Sucursal_Motocicleta.objects.filter(sucursal=sucursal)
			context = {
				'motocicletas':motocicletas,
				'sucursal':sucursal,
				'sucursal_motocicletas':sucursal_motocicletas
			}
			html = template.render(context)
			response = {
				'status':True,
				'html':html
			}
			data = json.dumps(response)
			return HttpResponse(data,content_type='application/json')

		template = loader.get_template('includes/form.html')
		context = {'form':form}
		html = template.render(context)
		response = {
			'status':False,
			'html':html
		}
		data = json.dumps(response)
		return HttpResponse(data, content_type='application/json')


class MotocicletaSucursalAjaxUpdateView(TemplateView):

	def get(self,request,*args,**kwargs):

		sucursal_motocicleta = Sucursal_Motocicleta.objects.get(id=kwargs['pk'])

		template = loader.get_template('includes/form.html')
		form = MotocicletaSucursalCreateForm(instance=sucursal_motocicleta)
		context = {'form':form}
		html = template.render(context)
		response = {
			'status':True,
			'html':html
		}

		data = json.dumps(response)
		return HttpResponse(data,content_type='application/json')

	def post(self,request,*args,**kwargs):
		sucursal_motocicleta = Sucursal_Motocicleta.objects.get(id=kwargs['pk'])
		form = MotocicletaSucursalCreateForm(
			request.POST,
			instance=sucursal_motocicleta)
		if form.is_valid():
			form.save()
			template = loader.get_template('vehiculo/includes/inventario.html')
			motos = Motocicleta.objects.all()
			sucursal = sucursal_motocicleta.sucursal
			sucursal_motocicleta = Sucursal_Motocicleta.objects.filter(sucursal=sucursal)
			context = {
				'motocicletas':motos,
				'sucursal':sucursal,
				'sucursal_motocicleta':sucursal_motocicleta
			}
			html = template.render(context)
			response = {
				'status':True,
				'html':html
			}
			data = json.dumps(response)
			return HttpResponse(data,content_type='application/json')

		template = loader.get_template('includes/form.html')
		context = {'form':form}
		html = template.render(context)
		response = {
			'status':False,
			'html':html
		}
		data = json.dumps(response)
		return HttpResponse(data, content_type='application/json')
