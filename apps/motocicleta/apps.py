from django.apps import AppConfig


class MotocicletaConfig(AppConfig):
    name = 'motocicleta'
