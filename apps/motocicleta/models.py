from django.db import models

# Create your models here.
from django.db import models

#Creacion del Choices para 'tipo' de motor de la motocicleta
MECANICA = 'Mecanica'
AUTOMATICA = 'Automatica'
tipo_motor = (
	(MECANICA, 'Mecanica'),
	(AUTOMATICA, 'Automatica'),
)

#Clase para ma categoria de la motocicleta
tipo_moto = (
	('CIUDAD', (
		('estandar', 'ESTANDAR'),
		('policia', 'POLICIA'),
		)
	),
	('CARRETERA', (
		('chappier', 'CHAPPIER'),
		('ciclomoto', 'CICLOMOTO'),
		('crucero', 'CRUCERO'),
		('custom', 'CUSTOM'),
		('deportiva', 'DEPORTIVA'),
		('noked', 'NOKED'),
		('turismo', 'TURISMO'),
		('velocidad', 'VELOCIDAD'),
		('sport_tunning', 'SPORT_TUNNING'),
		)
	),
	('CAMPO',(
		('cross','CROSS'),
		('enduro','ENDURO'),
		('super_cross','SUPER_CROSS'),
		('trial','TRIAL'),
		('todo_terreno','TODO_TERRONO'),
		)
	),
	('TRABAJO',(
		('moto_camioneta','MOTO_CAMIONETA'),
		)
	),
	)

#Clase Motocicleta
class Motocicleta(models.Model):
	#Numero de serie que identifica a la motocicleta
	numero_serie = models.CharField(null=True, blank=True, max_length=100)
	#Marca de la motocicleta
	marca = models.CharField(null=True, blank=True, max_length=100)
	#Precio de la motocicleta
	precio = models.FloatField(null=True, blank=True)
	#Modelo de la motocicleta / es decir el año que salio
	modelo = models.CharField(null=True, blank=True, max_length=100)
	#Potencia entregada por el motor
	potencia = models.CharField(null=True, blank=True, max_length=100)
	#nombre del motor
	motor = models.CharField(choices=tipo_motor, max_length=200)
	#Caracteristicas adicionales de la motocicleta
	caracteristicas = models.TextField(null=True, blank=True)
	#Imagen representativa de la motocicleta
	#Capacidad expresada en carga o pasajeros
	capacidad = models.CharField(null=True, blank=True, max_length=50)
	#Tipo del motocicleta
	tipo = models.CharField(max_length=50, choices=tipo_moto)
	imagen = models.ImageField(null=True,blank=True,upload_to = "imagenes/vehiculos/")

	#Permite determinar una representacion en string del objeto empleado
	def __str__(self):
		return self.marca + " " + self.modelo

	#Permite hacer modificaciones agregadas a la representacion del modelo
	class Meta:
		ordering = ['numero_serie']
		verbose_name_plural = "Motocicletas"






