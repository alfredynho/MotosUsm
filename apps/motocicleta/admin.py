# -*- encoding: utf-8 -*-

from django.contrib import admin
from .models import Motocicleta

class Vehiculo_Motocicleta(admin.ModelAdmin):

	list_display = (
		'numero_serie', 'marca', 'precio', 'modelo', 'potencia', 'motor',
		'caracteristicas', 'imagen', 'capacidad', 'tipo')

	search_fields = ('motocicleta', 'unidades', 'color')

admin.site.register(Motocicleta, Vehiculo_Motocicleta)
