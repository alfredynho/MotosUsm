# -*- encoding: utf-8 -*-
from django.db import models
import datetime
from apps.empleado.models import Empleado
from apps.cliente.models import Cliente
from apps.motocicleta.models import Motocicleta


# Forma de pago en que se realiza la compra
EFECTIVO = 'Efectivo'
#solo se encuentra habilitado la de efectivo
forma_pago_choices = (
	(EFECTIVO, 'Efectivo'),
)

#
class Cotizacion(models.Model):
	"""Define la organizacion del los datos de una cotizacion en la base de datos."""

	#Empleado que realiza la cotizacion, relacion uno a muchos
	empleado= models.ForeignKey(Empleado,default=None)
	#Cliente que solicita la cotizacion, relacion uno a muchos
	cliente= models.ForeignKey(Cliente,default=None)
	#Motocicleta que va a ser cotizada, relacion uno a muchos
	moto= models.ForeignKey(Motocicleta,default=None)
	#Fecha en que se realiza la cotizacion
	fecha=models.DateField(auto_now_add=True)
	#Fecha de vencimiento de la cotizacion
	fecha_vencimiento=models.DateField()
	#Forma de pago en la que se realiza la cotizacion
	forma_pago = models.CharField(max_length=20,choices=forma_pago_choices, default=EFECTIVO)
	#Estado de la cotizacion, Activa/inactiva
	habilitado = models.BooleanField(default = True)

	#Permite hacer modificaciones agregadas a la representacion del modelo
	class Meta:
		ordering = ['fecha']
		verbose_name_plural = "Cotizaciones"

	#Permite determinar una representacion en string del objeto
	def __str__(self):
		return self.cliente.nombre





