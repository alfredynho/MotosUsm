# -*- encoding: utf-8 -*-

from django.db import models
from datetime import datetime
from apps.empleado.models import Empleado
from apps.cliente.models import Cliente
from apps.sucursal.models import Sucursal_Motocicleta



# Forma de pago en que se realiza la compra
EFECTIVO = 'Efectivo'
forma_pago_choices = (
	(EFECTIVO, 'Efectivo'),
)

class Venta(models.Model):
	#Llave foranea al empleado que realiza la venta
	empleado = models.ForeignKey(Empleado,related_name='ventas')
	#Cliente que realiza la compra
	cliente = models.ForeignKey(Cliente)
	#motocicleta de la venta
	sucursal_motocicleta = models.ForeignKey(Sucursal_Motocicleta,related_name='ventas', default=None)
	#Fecha en que se realiza la venta
	fecha_venta=models.DateField(auto_now_add=True)
	#Precio final de la venta, precio motocicleta - descuento
	precio_venta = models.FloatField()
	#Forma de pago para la compra de la motocicleta
	forma_pago = models.CharField(max_length=20, choices=forma_pago_choices, default=EFECTIVO)
	#Estado de la venta, Activa/inactiva
	habilitado = models.BooleanField(default = True)

	#Permite hacer modificaciones agregadas a la representacion del modelo
	class Meta:
		ordering = ['fecha_venta']
		verbose_name_plural = "Ventas"

	#Permite determinar una representacion en string del objeto empleado
	def __str__(self):
		return self.sucursal_motocicleta.sucursal.nombre + " " + self.sucursal_motocicleta.motocicletas.marca


	@staticmethod
	def dinero_acumulado(self):
		ventas = self.objects.all()
		total = 0
		for venta in ventas:
			total += venta.precio_venta
		return total

	def nombre_sucursal(self):
		return self.sucursal_motocicleta.sucursal.nombre

	def marca_Motocicleta(self):
		return self.sucursal_motocicleta.motocicletas.marca
