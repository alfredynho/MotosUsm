from django.apps import AppConfig


class FacturaOrdenDeTrabajoConfig(AppConfig):
    name = 'factura_orden_de_trabajo'
