# -*- encoding: utf-8 -*-

from django.contrib import admin
from .models import Repuesto,create_tipo_repuesto

class AdminRepuesto(admin.ModelAdmin):

	list_display = (
		'id', 'nombre', 'precio',
		'marca', 'clasificacion',
		'imagen', 'proveedor', 'descripcion' )

	search_fields = ('id', 'nombre')

class AdminTipoRepuesto(admin.ModelAdmin):
	list_display = (
		'nombre'
		)
	search_fields =('nombre')

admin.site.register(Repuesto, AdminRepuesto)
admin.site.register(create_tipo_repuesto)

