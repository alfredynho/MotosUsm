# -*- encoding: utf-8 -*-

from django.db import models
from apps.proveedor.models import Proveedor

#creacion de la clase para los proveedores
class create_tipo_repuesto(models.Model):
	nombre = models.CharField(max_length=50,blank=True,null=True)

	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Crear tipo Repuesto"

	def __str__(self):
		return self.nombre

class Repuesto(models.Model):
	"""Define la organizacion del los datos de un repuesto en la base de datos."""

	#Django por defecto, cuando los modelos no tienen primary_key, coloca una llamada "id"

	#Nombre del repuesto
	nombre = models.CharField(null=True,blank=True,max_length=50)
	#Precio del repuesto
	precio = models.FloatField(null=True,blank=True)
	#Marca del repuesto
	marca = models.CharField(null=True,blank=True,max_length=20)
	#Clasificacion del repuesto
	clasificacion = models.OneToOneField(create_tipo_repuesto)
	#Provedor del repuesto
	proveedor = models.ForeignKey(Proveedor,default=None)
	#Descripcion del repuesto
	descripcion = models.CharField(null=True,blank=True,max_length=100)
	#Imagen del repuesto
	imagen = models.ImageField(null=True,blank=True,upload_to = "imagenes/repuestos/")
	#Thumbnail que permite reducir la imagen del repuesto

	#Permite hacer modificaciones agregadas a la representacion del modelo
	class Meta:
		ordering = ['nombre']
		verbose_name_plural = "Repuestos"
		unique_together = ("nombre", "proveedor")


	#Permite determinar una representacion en string del objeto repuesto
	def __str__(self):
		return self.nombre