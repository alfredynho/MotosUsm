# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-15 15:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('proveedor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Repuesto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=50, null=True)),
                ('precio', models.FloatField(blank=True, null=True)),
                ('marca', models.CharField(blank=True, max_length=20, null=True)),
                ('clasificacion', models.CharField(blank=True, choices=[('Luminarias', 'Luminarias'), ('Ferreteria', 'Ferreteria'), ('Pinturas', 'Pinturas'), ('Rodamientos', 'Rodamientos'), ('Solventes', 'Solventes'), ('Bandas_cadenas', 'Bandas y cadenas'), ('Limpieza', 'Limpieza'), ('Sellos_empaques', 'Sellos y empaques'), ('Lubricantes', 'Lubricantes'), ('Guiñadores', 'Guiñadores'), ('Cadenas', 'Cadenas'), ('Catalina', 'Catalina'), ('Mataperros', 'Mataperros'), ('Embrague', 'Embrague'), ('Freno', 'Freno')], default='Freno', max_length=20, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=100, null=True)),
                ('imagen', models.ImageField(blank=True, null=True, upload_to='imagenes/repuestos/')),
                ('proveedor', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='proveedor.Proveedor')),
            ],
            options={
                'ordering': ['nombre'],
                'verbose_name_plural': 'Repuestos',
            },
        ),
        migrations.AlterUniqueTogether(
            name='repuesto',
            unique_together=set([('nombre', 'proveedor')]),
        ),
    ]
