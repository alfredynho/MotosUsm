# -*- encoding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin
from apps.home.views import Login,Logout

urlpatterns = [
	url(r'^$', Login.as_view(), name='login'),
	url(r'^inicio/login/$', Login.as_view(), name='login'),
	url(r'^inicio/logout/$', Logout.as_view(), name='logout'),
]
