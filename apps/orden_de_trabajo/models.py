# -*- encoding: utf-8 -*-

from django.db import models
from apps.empleado.models import Empleado
from apps.cliente.models import Cliente
from apps.sucursal.models import Sucursal
from apps.motocicleta.models import Motocicleta


#Estados de la reparacion de la motocicleta

#La motocicleta esta en el taller, pero no se ha revisado se encuentra en estado 'PENDIENTE'
PENDIENTE = 'Pendiente'
#Ya esta la cotización de los daños de la motocicleta se encuentra en estado 'COTIZADO'
COTIZADO = 'Cotizado'
#La motocicleta  esta reparado pero aun no ha sido entregado a su dueño
REPARADO = 'Reparado'
#La motocicleta fue reparada y entregada al cliente
REPARADO_Y_ENTREGADO = 'Reparado y entregado'
#La motocicleta fue retirada ya que el cliente no acepto su reparacion
RETIRADO = 'Retirado'

tipo_choice = (
	(PENDIENTE, 'Pendiente'),
	(COTIZADO, 'Cotizado'),
	(REPARADO, 'Reparado'),
	(REPARADO_Y_ENTREGADO, 'Reparado y entregado'),
	(RETIRADO, 'Retirado'),
 )

class OrdenDeTrabajo(models.Model):
	#Relacion de 1-n para orden con el empleado que la esta realizando
	empleado = models.ForeignKey(Empleado,default=None)
	#Sucursal a la que ingresa la motocicleta
	sucursal = models.ForeignKey(Sucursal,default=None)
	#dueno de la motocicleta que entra al taller, relacion uno a muchos
	cliente = models.ForeignKey(Cliente,default=None)
	#motocicleta que va a ser reparado
	motocicleta = models.ForeignKey(Motocicleta,default=None)
	#placa de la motocicleta que entra al taller
	placa = models.CharField(max_length=50)
	#Fecha de entrada al taller
	fecha_entrada = models.DateField(auto_now_add=True,blank=True, null=True)
	#Fecha de salida del taller
	fecha_salida = models.DateField(blank=True, null=True)
	#Estado de la motocicleta en el taller
	estado_reparacion = models.CharField(null=True,blank=True,max_length=50,choices=tipo_choice,default=PENDIENTE)
	#Observacion de los daños de la motocicleta
	observacion = models.TextField(null=True,blank=True,max_length=200)
	#Estado de la OrdenDeTrabajo, Activa/inactiva
	habilitado = models.BooleanField(default = True)

	#Permite hacer modificaciones agregadas a la representacion del modelo
	class Meta:
		ordering = ['fecha_entrada']
		verbose_name_plural = "Orden de Trabajo"
		unique_together = ('placa','fecha_salida')

	#Permite determinar una representacion en string del objeto repuesto
	def __str__(self):
		return self.motocicleta.marca

