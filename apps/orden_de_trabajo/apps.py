from django.apps import AppConfig


class OrdenDeTrabajoConfig(AppConfig):
    name = 'orden_de_trabajo'
