# -*- encoding: utf-8 -*-

from django.db import models
from apps.orden_de_trabajo.models import OrdenDeTrabajo
from apps.repuesto.models import Repuesto
from datetime import datetime

class CotizacionOrdenDeTrabajo(models.Model):
	#Orden de trabajo para la cual se relizara la cotizacion
	#related_name para poder acceder desde orden de trabajo a CotizacionOrdenDeTrabajo
	orden_de_trabajo = models.OneToOneField(OrdenDeTrabajo,related_name='cotizacion')
	#Costo de la reparacion de la motocicleta
	costo_reparacion = models.FloatField()
	#Fecha hasta la cual es valida la cotizacion que se realizo a la motocicleta
	fecha_vencimiento = models.DateField()
	#Se inhabilita una cotizacion cuando la motocicleta ha sido reparado
	#o el cliente retira la motocicleta sin haber sido reparado
	habilitado = models.BooleanField(default=True)

	class Meta:
		ordering = ['fecha_vencimiento']
		verbose_name_plural = "Cotizaciones Ordenes de Trabajo"
		#funcion para el nombre de la cotizacion y un id para la cotizacion
	def __str__(self):
		return "Cotizacion {}".format(self.id)

		#funcion para saber si es valida la fecha de la cotizacion de orden de trabajo
	def es_valida(self):
		""" Documentacion es_valida

			Determina se la cotizacion es valida o no se ha vencido
			hasta la fecha actual.
		"""
		if datetime.now().date() > self.fecha_vencimiento:
			return False
		return True

		#funcion para calcular el total de los precios de costo de repuestos
	def costo_repuestos(self):
		""" Documentacion calcular_costo_total_repuestos

			Determina el costo total de los repuesto implicados en la
			cotizacion de la reparacion de la motocicleta
		"""
		#costo totoal en 0
		costo_repuestos = 0
		#for para ver la cantidad de repuestos existentes del cliente
		for repuesto_cantidad in self.repuestos_cantidad.all():
			costo_repuestos += (repuesto_cantidad.repuesto.precio * repuesto_cantidad.cantidad)
		return costo_repuestos

		#funcion para sacar el costo total de la reparacion
	def costo_total(self):
		""" Documentacion calcular_costo_total_reparacion

			Determina el costo total de la reparacion de la motocicleta que es igual
			a costo de la reparacion + costo total en repuestos.
		"""

		return self.costo_reparacion + self.costo_repuestos()

# clase para la cantidad de repuestos que se esta requirimiento
class RepuestoCantidad(models.Model):
	# identificacion de la Cotizacion orden de trabajo
	# related_name para poder acceder desde una instancia de CotizacionOrdenDeTrabajo a RepuestoCantidad
	cotizacion_orden_de_trabajo = models.ForeignKey(CotizacionOrdenDeTrabajo, related_name='repuestos_cantidad')
	# Repuesto a usar
	repuesto = models.ForeignKey(Repuesto)
	# Cantidad del repuesto a usar en la reparación
	cantidad = models.IntegerField()

	class Meta:
		verbose_name_plural = "Repuestos Cantidad"
		unique_together = (("cotizacion_orden_de_trabajo", "repuesto"),)
