from django.apps import AppConfig


class CotizacionOrdeDeTrabajoConfig(AppConfig):
    name = 'cotizacion_orde_de_trabajo'
