from django.contrib import admin
from apps.cotizacion_orde_de_trabajo.models import CotizacionOrdenDeTrabajo,RepuestoCantidad
#Recording the models in the admin
class CotiAdmin(admin.ModelAdmin):
	list_display=('orden_de_trabajo','costo_reparacion','fecha_vencimiento','habilitado')

class RepCantidad_Admin(admin.ModelAdmin):
	list_display = ('cotizacion_orden_de_trabajo','repuesto','cantidad')

admin.site.register(CotizacionOrdenDeTrabajo,CotiAdmin)
admin.site.register(RepuestoCantidad,RepCantidad_Admin)